const pkg = require('./package');
require('dotenv').config();//eslint-disable-line

const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  mode: 'universal',
  dev: !isProd,
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'type/css', href: '/style.css' },
    ],
    noscript: [{ innerHTML: 'This website requires JavaScript' }],
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Router config
  */
  router: {},
  /*
  ** Global CSS
  */
  css: ['~/css/main.css'],
  /*
  ** Build configuration
  */
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    '@nuxtjs/component-cache',
    'nuxt-purgecss',
  ],
  /*
    ** Config manifest
    */
  manifest: {
    short_name: 'Sgr',
    name: 'S - Group\'s blog',
    start_url: '.',
    display: 'standalone',
    background_color: '#fff',
    description: 'A Blog of programming expertise',
    lang: 'vi-VN',
    orientation: 'portrait-primary',
  },
  modulesDir: ['../../node_modules'],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    analyze: !isProd,
    cache: !isProd,
    cssSourceMap: !isProd,
    optimization: {
      minimize: isProd,
    },
    extractCss: isProd,
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
  purgeCSS: {
    enabled: ({ isDev, isClient }) => (!isDev && isClient),
    styleExtensions: ['.css'],
    paths: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
    ],
    whitelist: ['body', 'html', 'nuxt-progress', 'active'],
    extractors: [
      {
        extractor: class {
          static extract(content) {
            return content.match(/[A-z0-9-:\\/]+/g);
          }
        },
        extensions: ['html', 'vue', 'js'],
      },
    ],
  },
  workbox: {
    importScripts: [
      '/custom-sw.js',
    ],
  },
};
